import cv2
import numpy as np


# # LOAD A VIDEO
def playVideo(video):
    frameWidth = 640
    frameHeight = 480
    cap = cv2.VideoCapture(video)
    while True:
        success, img = cap.read()
        img = cv2.resize(img, (frameWidth, frameHeight))
        cv2.imshow("Result", img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


# # READ FROM WEBCAM
def videoCamera():
    frameWidth = 640
    frameHeight = 480
    cap = cv2.VideoCapture(0)
    cap.set(3, frameWidth)
    cap.set(4, frameHeight)
    cap.set(10, 150)
    while True:
        success, img = cap.read()
        cv2.imshow("Result", img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


# # READ IMG WITH EFFECTS
def imageWithEffect():
    img = cv2.imread('Resources/margot.jpg')
    kernel = np.ones((2, 2), np.uint8)

    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    imgBlur = cv2.GaussianBlur(imgGray, (7, 7), 0)
    imgCanny = cv2.Canny(img, 150, 200)
    imgDialation = cv2.dilate(imgCanny, kernel, iterations=1)
    imgEroded = cv2.erode(imgDialation, kernel, iterations=1)

    # cv2.imshow("Gray Image", imgGray)
    # cv2.imshow("Blur Image", imgBlur)
    cv2.imshow("Canny Image", imgCanny)
    cv2.imshow("Dialation Image", imgDialation)
    cv2.imshow("Eroded Image", imgEroded)
    cv2.waitKey(0)

# # Warp perspective


def getWarpPerspective():
    img = cv2.imread("Resources/cards.jpg")
    width, height = 250, 350
    pts1 = np.float32([[485, 171], [645, 171], [504, 403], [707, 404]])
    pts2 = np.float32([[0, 0], [width, 0], [0, height], [width, height]])
    matrix = cv2.getPerspectiveTransform(pts1, pts2)
    imgOutput = cv2.warpPerspective(img, matrix, (width, height))

    cv2.imshow("Image", img)
    cv2.imshow("Output", imgOutput)
    cv2.waitKey(0)


# GET IMAGES CONTOURS AND NAMES
def stackImages(scale,imgArray):
    rows = len(imgArray)
    cols = len(imgArray[0])
    rowsAvailable = isinstance(imgArray[0], list)
    width = imgArray[0][0].shape[1]
    height = imgArray[0][0].shape[0]
    if rowsAvailable:
        for x in range ( 0, rows):
            for y in range(0, cols):
                if imgArray[x][y].shape[:2] == imgArray[0][0].shape [:2]:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (0, 0), None, scale, scale)
                else:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (imgArray[0][0].shape[1], imgArray[0][0].shape[0]), None, scale, scale)
                if len(imgArray[x][y].shape) == 2: imgArray[x][y]= cv2.cvtColor( imgArray[x][y], cv2.COLOR_GRAY2BGR)
        imageBlank = np.zeros((height, width, 3), np.uint8)
        hor = [imageBlank]*rows
        hor_con = [imageBlank]*rows
        for x in range(0, rows):
            hor[x] = np.hstack(imgArray[x])
        ver = np.vstack(hor)
    else:
        for x in range(0, rows):
            if imgArray[x].shape[:2] == imgArray[0].shape[:2]:
                imgArray[x] = cv2.resize(imgArray[x], (0, 0), None, scale, scale)
            else:
                imgArray[x] = cv2.resize(imgArray[x], (imgArray[0].shape[1], imgArray[0].shape[0]), None,scale, scale)
            if len(imgArray[x].shape) == 2: imgArray[x] = cv2.cvtColor(imgArray[x], cv2.COLOR_GRAY2BGR)
        hor= np.hstack(imgArray)
        ver = hor
    return ver

def getContours(img):
    contours,hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > 500:

            cv2.drawContours(imgContour, cnt, -1, (255, 0, 0), 3)
            peri = cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, 0.02*peri, True)
            objCor = len(approx)
            x,y,w,h = cv2.boundingRect(approx)

            if objCor == 3: objectType = "Triangle"
            elif objCor == 4:
                if area > 3000 and area < 4500: objectType = "Diamond"
                else: objectType = "Square"
            elif objCor > 4 and objCor < 7: objectType = "House"
            elif objCor > 6: objectType = "Circle"
            else: objectType = "None"

            if objectType == "Circle":
                print('Type:', objectType)
                print('Corners: ', objCor)
                print('Area: ', area)

            cv2.rectangle(imgContour, (x, y), (x+w, y+h), (0, 255, 0), 2)
            cv2.putText(imgContour, objectType, (x+(w//2) - 10, y+(h//2) + 10), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0), 2)




path = 'Resources/shapes.PNG'
img = cv2.imread(path)
imgContour = img.copy()

imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
imgBlur = cv2.GaussianBlur(imgGray, (7, 7), 1)
imgCanny = cv2.Canny(imgBlur, 50, 50)
getContours(imgCanny)

imgBlank = np.zeros_like(img)
imgStack = stackImages(0.8, ([img, imgGray, imgBlur],
                             [imgCanny, imgContour, imgBlank]))

cv2.imshow("Stack", imgStack)
cv2.waitKey(0)
